function repetir(){
    var text = "";
    var mult = 0;
    for(var v = 0; v<11; v++){
        for(var r = 0; r<11; r++){
            mult = v * r;
            text += v + " * " + r + " = " + mult + "<br>";
        }
        text += "<hr>";
    }
    document.getElementById('resultado').innerHTML = text;
}
window.onload = repetir()