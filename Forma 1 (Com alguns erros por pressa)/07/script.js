function ligarDesliga(){

    var imagem = document.getElementById('lampada').src;
    var imagem_desligado = 'https://github.com/gabrieldarezzo/helpjs-ravi/blob/master/images/lampada.jpg?raw=true';
    var imagem_ligado = 'https://github.com/gabrieldarezzo/helpjs-ravi/blob/master/images/lampada-on.jpg?raw=true';
    
    if(imagem == imagem_ligado){
    	document.getElementById('lampada').src = imagem_desligado;
    }else{
    	document.getElementById('lampada').src = imagem_ligado;
    }
}