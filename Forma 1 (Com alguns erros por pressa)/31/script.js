function load() {
  var zero = 0;
  var soma = 0;
  var text = "";
  while (zero < 11) {
    for (var r = 0; r < 11; r++) {
      soma = zero * r;
      text += zero + " * " + r + " = " + soma + "<br>";
    }
    if (zero === 0)
      document.querySelector("#zero").innerHTML = text + "<hr>";
    else if (zero === 1)
      document.querySelector("#um").innerHTML = text + "<hr>";
    else if (zero === 2)
      document.querySelector("#dois").innerHTML = text + "<hr>";
    else if (zero === 3)
      document.querySelector("#tres").innerHTML = text + "<hr>";
    else if (zero === 4)
      document.querySelector("#quatro").innerHTML = text + "<hr>";
    else if (zero === 5)
      document.querySelector("#cinco").innerHTML = text + "<hr>";
    else if (zero === 6)
      document.querySelector("#seis").innerHTML = text + "<hr>";
    else if (zero === 7)
      document.querySelector("#sete").innerHTML = text + "<hr>";
    else if (zero === 8)
      document.querySelector("#oito").innerHTML = text + "<hr>";
    else if (zero === 9)
      document.querySelector("#nove").innerHTML = text + "<hr>";
    else
      document.querySelector("#dez").innerHTML = text + "<hr>";
    text = "";
    zero++;
  }
}
window.onload = load;