function somar() {
    var soma1 = document.querySelector("#soma1").value;
    var soma2 = document.querySelector("#soma2").value;
    var resultado;
    var resto;
    if (soma1 != "" && soma2 != "")
        resultado = parseInt(soma1) / parseInt(soma2),
        resto = parseInt(soma1) % parseInt(soma2);
    else
        resultado = 0,
        resto = 0;
    document.querySelector("#resultado-soma").innerHTML = "Resultado: " + resultado;
    document.querySelector("#resto").innerHTML = "Resto: " + resto;
}