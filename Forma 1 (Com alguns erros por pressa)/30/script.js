function repetir() {
    var text = "";
    var r = 0;
    while (r < 21) {
        var resto = r % 2;
        if (resto === 0)
            text += r + ", ";
        r++
    }
    document.querySelector("#resultado").innerHTML = text;
}
function repetirfor() {
    var text = "";
    for (var r = 0; r < 21; r++) {
        var resto = r % 2;
        if (resto === 0)
            text += r + ", ";
    }
    document.querySelector("#resultado").innerHTML = text;
}